/**
* Util Class
* 
* @class
*/

/**
* Boolean to determine whether we are running a Jest test or not
* @private
* @constant
* @type {Boolean}
*/
const IS_TEST = typeof process != 'undefined' && process.env.JEST_WORKER_ID;

/*
* Imports
*/
import { ShowToastEvent } from 'lightning/platformShowToastEvent';

/**
* Enum for Toast Message Variants
* @private
* @enum {String}
*/
const toastVariants = {
    /** Returns "error" */
    error: 'error',
    /** Returns "warning" */
    warning: 'warning',
    /** Returns "success" */
    success: 'success',
    /** Returns "info" */
    info: 'info'
};

/**
* Enum for Toast Message Modes
* @private
* @enum {String}
*/
const toastModes = {
    /** Returns "dismissable" */
    dismissable: 'dismissable',
    /** Returns "pester" */
    pester: 'pester',
    /** Returns "sticky" */
    sticky: 'sticky'
};

/**
* Executes the ShowToastEvent to display a Toast message
* 
* @returns {void} No return value required
* 
* @param {toastVariants} variant Toast Variant enum
* @param {String} title Title to be displayed
* @param {String} message Message to be displayed
* @param {toastModes} [mode] Toast Message mode to be displayed
* @param {Object} [messageData] Object containing the Message data to be merged into the message
*
* @example
* // @wire response error to be throws
* showToast('error', 'An error occurred', resp.error);
* @example
* // Custom info message to be displayed, without a title
* showToast('info', null, 'Custom error in here.');
* @example
* // Display a Warning message with a dismissable mode
* showToast('warning', 'Warning!', 'You have lef this page unattended for 5 minutes, you will be logged out if left for another 5!', 'dismissable');
* @example
* // Display a successful message when you create a record, numbers in brackets are replaced by the value from the array within the same index
* showToast('success', 'Success!',
*     'Record {0} created! See it {1}!', null, [
*     'Salesforce',
*     {
*         url: url,
*         label: 'here'
*     }
* ]);
*/
export const showToast = (variant, title, message, mode, messageData) => {
    // Instantiate error Array
    let errors = [];
    // If toastVariants does not contain the variant passed, add an error to the errors Array
    if (!toastVariants.hasOwnProperty(variant))
        errors.push('Toast variant "' + variant + '" is invalid');
    // If toastModes does not contain the mode passed, add an error to the errors Array
    if (mode && !toastModes.hasOwnProperty(mode))
        errors.push('Toast mode "' + mode + '" is invalid');
    // If errors lenght is greater than 0, throw custom exception Object
    if (errors.length > 0)
        throw Object.create({ message: 'A "Toast Message" error occurred.', errors: errors });

    // If we are running a test, set console.log, otherwise set dispatchEvent, workaround for "Illegal Invocation" of dispatchEvent in a test context
    const triggerEvt = IS_TEST ? console.log : dispatchEvent;

    // Dispatch Toast Event
    triggerEvt(
        new ShowToastEvent({
            title: title,
            message: message,
            variant: variant,
            mode: (mode ? mode : 'dismissable'),
            messageData: (messageData ? messageData : null)
        })
    );
};

/**
* Show error toast
*
* @returns {void} No return value required
*
* @param {String} message The message for show
*/
export const showToastError = message => showToast('error', 'An Error Occurred', message, 'dismissable');

/**
* Returns the Message String from an AuraHandledException error message being returned from a server side request
* 
* @return {String} Returns a String that has been parsed from Error the Object
* 
* @param {Object} obj The Object to be parsed
* 
* @example
* // Parses the Object to retrieve the Error Message
* getError(response.error);
* @example
* // This would not be parsed as it is a String as it is not an Object and cannot be parsed
* getError('Error Message');
*/
export const getError = (obj) => {
    if (obj.hasOwnProperty('body') && isObject(obj.body) && obj.body.hasOwnProperty('message')) {
        return obj.body.message;
    } else if (obj.hasOwnProperty('body') && isArray(obj.body)) {
        let err = [];
        for (let i in obj.body) {
            if (obj.body[i].message !== null)
                err.push('\u2022 ' + obj.body[i].message);
        }
        return err.join('\r\n');
    }
    return obj;
};

/**
* Object to store URL Params
* @private
* @type {Object}
*/
let params = {};

/**
* Prepares an Object containing all of the URL Params
* 
* @return {} No Data is returned
* 
* @example
* // Preps the Params Object
* getUrlParams();
*/
export const getUrlParams = () => {
    // If we are in a test context, set a search, otherwise get the locations search
    let search = IS_TEST ? '?param1=1&param2=2' : location.search;

    // If the search is not blank
    if (!isBlank(search)) {
        // Prep URL Params
        params = JSON.parse('{"' + search.substring(1, search.length).replace(/&/g, '","').replace(/=/g, '":"') + '"}', (key, value) => {
            return key === "" ? value : decodeURIComponent(value);
        });
    }
};

/**
* Returns a Boolean to determine whether the any of the values are Null, Undefined, or an empty Strings
* 
* @return {String} Returns the value related to the URL Param passed
* 
* @param {Array} vals The Property of the Object used to return the relevant value
* 
* @example
* // Returns the value associated with the "key" Property
* getUrlParam('key');
*/
export const getUrlParam = val => {
    if (Object.keys(params).length === 0)
        getUrlParams();

    return params.hasOwnProperty(val) ? params[val] : null;
};

/**
* Returns a Boolean to determine whether the value is null or undefined only.
* 
* @return {Boolean} Returns a Boolean to determine whether the value is null or undeinfed
* 
* @param {String} val The value to be checked
* 
* @example
* // Returns true
* isNullOrUndefined(null);
* @example
* // Returns false
* isNullOrUndefined('String');
*/
export const isNullOrUndefined = (val) => {
    return val === null || val === undefined;
};
