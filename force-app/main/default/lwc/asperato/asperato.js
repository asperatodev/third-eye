import { LightningElement, api, wire } from 'lwc';

// Util Methods
import { showToastError, getError, getUrlParam, isNullOrUndefined, showToast } from './util';

// Custom Labels
import ENCRYPTION_KEY from '@salesforce/label/c.Encryption_Key';

// Apex Server-Side Methods
import getSettingData from '@salesforce/apex/Asperato.getSettingData';
import getRecord from '@salesforce/apex/Asperato.getRecord';
import createRecord from '@salesforce/apex/Asperato.createRecord';
import decrypt256 from '@salesforce/apex/EncryptionUtil.decrypt256';

// Authorisation Schema Info
import AUTHORISATION_OBJECT from '@salesforce/schema/asp04__Authorisation__c';
import ACCOUNT_FIELD from '@salesforce/schema/asp04__Authorisation__c.Account__c';
import PAYMENT_ROUTE_OPTIONS_FIELD from '@salesforce/schema/asp04__Authorisation__c.asp04__Payment_Route_Options__c';

// Payment Schema Info
import PAYMENT_OBJECT from '@salesforce/schema/asp04__Payment__c';

/**
 * Asperato
 * 
 * @class
 */
export default class Asperato extends LightningElement {
    // Public reactive variables
    @api height;
    @api width;
    @api
    get aid() {
        if (this.loading === false)
            return this._aid;
        return null;
    }
    set aid(val) {
        this._aid = val;
    }

    @api
    get pid() {
        if (this.loading === false)
            return this._pid;
        return null;
    }
    set pid(val) {
        this._pid = val;
    }

    // Private variables
    paymentComplete;
    encryptedString;
    encryptionKey;
    paymentLoaded;
    paymentsSrc;
    authLoaded;
    returnUrl;
    legacyAid;
    complete;
    settings;
    loading;
    data;
    _aid;
    _pid;


    /**
     * Execute on LWC Connection/Initialisation
     * 
     * @returns {void}
     */
    connectedCallback() {
        // Set default values
        this.encryptedString = decodeURIComponent(getUrlParam('p'));
        this.returnUrl = getUrlParam('return-to') ? getUrlParam('return-to') : 'https://www.crunch.co.uk';
        this.encryptionKey = ENCRYPTION_KEY;
        this.paymentComplete = false;
        this.complete = false;
        this.loading = true;

        this.prepListener();
    }

    /**
     * Wire method to get Custom Setting data
     * 
     * @returns {Object} Returns an Object containing an error or data value
     */
    @wire(getSettingData)
    setSettings({ error, data }) {
        if (error) {
            showToastError(getError(error));
        } else if (data) {
            this.settings = data;
        }
    }

    /**
     * Wire method to decrypt the encrypted URL Param
     * 
     * @returns {Object} Returns an Object containing an error or data value
     * 
     * @param {Object} encryptedString The Encrypted String to be decrypted
     * @param {Object} key The Key to be used with the decryption process
     */
    @wire(decrypt256, { encryptedString: '$encryptedString', key: '$encryptionKey' })
    setData({ error, data }) {
        if (error) {
            console.log(106, error);
            showToastError(getError(error));
        } else if (data) {
            this.data = data;

            if (this.data.hasOwnProperty('legacyAid')) {
                this.legacyAid = this.data.legacyAid;
            } else {
                this.aid = this.data.aid;
                this.pid = this.data.pid;

                this.loading = false;
            }
        }
    }

    /**
     * Wire method to get the Legacy Authorisation record
     * 
     * @returns {Object} Returns an Object containing an error or data value
     * 
     * @param {Object} recordId Id of the Legacy Authorisation to clone
     */
    @wire(getRecord, { recordId: '$legacyAid' })
    setLegacyRecord({ error, data }) {
        if (error) {
            console.log(125, error);
            showToastError(getError(error));
        } else if (data) {
            this.cloneRecord(data);
        }
    }

    /**
     * Method to clone an Authorisation record, then create a Payment (where applicable)
     * 
     * @returns {void}
     * 
     * @param {Object} data An Object containing the queries Authorisation record
     */
    cloneRecord(data) {
        // Prepare recordIput
        const recordInput = {
            sobjectType: AUTHORISATION_OBJECT.objectApiName
        }

        // Set field values
        recordInput[ACCOUNT_FIELD.fieldApiName] = data.Account__c || null;
        recordInput[PAYMENT_ROUTE_OPTIONS_FIELD.fieldApiName] = data.asp04__Payment_Route_Options__c || null;

        // Invoke the createRecord method
        createRecord({ record: recordInput })
            .then(data => {
                // Set the returned records Id
                this._aid = data.Id;

                // If there is a paymentRecord present
                if (this.data.hasOwnProperty('paymentRecord') && !isNullOrUndefined(this.data.paymentRecord)) {
                    // Prepare paymentRecord
                    const paymentRecord = {
                        sobjectType: PAYMENT_OBJECT.objectApiName
                    };

                    // Iterate over the Keys in the paymentRecord Object, then iterate over them
                    Object.keys(this.data.paymentRecord).forEach(field => {
                        // If the field is not attributes, set the field data
                        if (field !== 'attributes')
                            paymentRecord[field] = this.data.paymentRecord[field];
                    });

                    // Invoke the createRecord method
                    createRecord({ record: paymentRecord })
                        .then(data => {
                            // Set the returned records Id
                            this._pid = data.Id;

                            // Set loading to false
                            this.loading = false;
                        })
                        .catch(error => {
                            console.log(163, error);
                            showToastError(getError(error))
                        });
                } else {
                    this.loading = false;
                }
            })
            .catch(error => {
                console.log(171, error);
                showToastError(getError(error))
            });
    }

    /**
     * Method to prepare a listener for Asperato JS messages
     * 
     * @returns {void}
     */
    prepListener() {
        // Add Event Listener to wait for any messages being posted
        window.addEventListener('message', event => {
            // Get message data
            const data = event.data;

            console.log(data);

            switch (data) {
                // If data is Exit (Finish) Screen about to display
                case 'asp--exit-screen':
                    if (!isNullOrUndefined(this.pid) && this.paymentComplete === false) {
                        // Set paymentComplete to true
                        this.paymentComplete = true;

                        // Scroll back to the top of the page
                        scrollTo(0, 0);
                    } else {
                        this.redirect();
                    }

                    break;
                // If data is Complete (Finish) Button clicked
                case 'asp--complete':
                    if (this.paymentComplete) {
                        // Set complete to true
                        this.complete = true;

                        // Scroll back to the top of the page
                        scrollTo(0, 0);
                    }
                    break;
                default:
                    // Scroll back to the top of the page
                    scrollTo(0, 0);

                    break;
            }
        });
    }

    /**
     * Method to flag when the Payment iFrame has loaded
     * 
     * @returns {void}
     */
    paymentReady() {
        this.paymentLoaded = true;
    }

    /**
     * Method to flag when the Authorisation iFrame has loaded
     * 
     * @returns {void}
     */
    authReady() {
        this.authLoaded = true;
    }

    redirect() {
        // Display Toast message with redriect url
        showToast(
            'success',
            'Thank you!',
            'Your payment setup is complete, please hold tight whilst we redirect you. \r\n \r\n If you are not redirected within a few seconds, please click {0}.',
            'sticky',
            [
                {
                    url: this.returnUrl,
                    label: 'here'
                }
            ]
        );

        // Set Timeout
        setTimeout(() => {
            // Use location.href here over NavigationMixin, as NavigationMixin opens in a new tab, we want to overwrite the current tab
            location.href = this.returnUrl;
        }, 2000);
    }

    /**
     * Get method to return the Payment URL for the iFrame
     * 
     * @returns {String} The actual URL
     */
    get paymentSrc() {
        if (this.showPayment)
            return `https://${this.settings.asp04__Server_URL__c}/PMWeb1?pmRef=${this.settings.asp04__Customer_ID__c}&pid=${this.pid}`;
        return null;
    }

    /**
     * Get method to return the Authorisation URL for the iFrame
     * 
     * @returns {String} The actual URL
     */
    get authorisationSrc() {
        if (this.showAuth)
            return `https://${this.settings.asp04__Server_URL__c}/PMWeb1?pmRef=${this.settings.asp04__Customer_ID__c}&aid=${this.aid}`;
        return null;
    }

    /**
     * Get method to return a Boolean to determine when the Authorisation iFrame should display
     * 
     * @returns {Boolean}
     */
    get showAuth() {
        return !isNullOrUndefined(this.settings)
            &&
            !isNullOrUndefined(this.aid)
            &&
            (
                isNullOrUndefined(this.pid)
                ||
                (
                    !isNullOrUndefined(this.pid)
                    &&
                    !isNullOrUndefined(this.paymentComplete)
                    &&
                    this.paymentComplete
                )
            );
    }

    /**
     * Get method to return a Boolean to determine when the Payment iFrame should display
     * 
     * @returns {Boolean}
     */
    get showPayment() {
        return !isNullOrUndefined(this.settings)
            &&
            !isNullOrUndefined(this.pid)
            &&
            !isNullOrUndefined(this.paymentComplete)
            &&
            !this.paymentComplete;
    }

    /**
     * Get method to return CSS, which controls the display of the Lightning Spinner over the iFrame area
     * 
     * @returns {String} CSS used on a Div
     */
    get style() {
        return `position: relative; height: ${this.height}`;
    }
}
