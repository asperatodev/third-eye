import { LightningElement, api } from 'lwc';

export default class FlowIFrame extends LightningElement {
    @api src;
    @api width;
    @api height;

    ready;

    loaded() {
        this.ready = true;
    }


    get style() {
        return `border: none !important; width: ${this.width}; height: ${this.height}; position: relative; left: calc((100% - ${this.width}) / 2);`;
    }
}