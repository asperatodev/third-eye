/*
 * @who		: Timothy Gentet-O'Brien <tim@thirdeyeconsulting.co.uk>
 * @when	: 4 Sep 2019
 * @what	: Class used to serve data to the Asperato Custom iFrame LWC
 */
public without sharing class Asperato {
    /*
     * @method		: getSettingData()
     * @params		: N/A
     * @return 		: Asperato One Settings - Custom Setting Record
     * @description	: Method to return Custom Setting data to the LWC
     */
	@AuraEnabled(cacheable = true)
    public static asp04__AsperatoOneSettings__c getSettingData(){
        return asp04__AsperatoOneSettings__c.getInstance();
    }
    
    /*
     * @method		: getSettingData()
     * @params		: N/A
     * @return 		: Asperato One Settings - Custom Setting Record
     * @description	: Method to return Custom Setting data to the LWC
     */
	@AuraEnabled(cacheable = true)
    public static sObject getRecord(Id recordId, String childRelationship, String childSortField, String childSortOrder, Integer childLimit){
        // Get all field, using the recordId to get the sObject info
        List<String> fields = new List<String>(
            recordId.getSobjectType().getDescribe().fields.getMap().keySet()
        );
        
        // Instanitate a new List of String
        List<String> childFields = new List<String>();
        
        // If the childRelationship variable is not blank
        if(String.isNotBlank(childRelationship)){
            // Iterate over the child relationships of the current sObject
            for(Schema.ChildRelationship s :recordId.getSobjectType().getDescribe().getChildRelationships()){
                // If the relationship name matches
                if(s.getRelationshipName() == childRelationship){
                    // Get all fields form the Child sObject
                    childFields = new List<String>(s.getChildSObject().getDescribe().fields.getMap().keySet());
                    
                    // Break to stop processing
                    break;
                }
            }
        }
        
        // Define a String
        String childQuery;
        
        // If childFields is not empty
        if(!childFields.isEmpty()){
            // Prep the base Query
            childQuery = 'SELECT ' + String.join(childFields, ', ') + ' ' + 'FROM ' + childRelationship;
            
            // If the childSortField is not blank
            if(String.isNotBlank(childSortField)){
                // Set Order By
                childQuery += ' ORDER BY ' + childSortField;
                
                // If the childSortOrder is not blank, set the Order By direction
                if(String.isNotBlank(childSortOrder))
                    childQuery += ' ' + childSortOrder;
            }
            
            // If the childLimit is not null and is greater than 0, set the Limit
            if(childLimit != null && childLimit > 0)
                childQuery += ' LIMIT ' + childLimit;
        }
        
        // Query and return the sObject, with child records (where applicable)
        return Database.query(
            'SELECT ' + String.join(fields, ', ') + ' ' +
            (
                String.isNotBlank(childQuery) ? ',(' + childQuery + ')' : ''
            ) +
            'FROM ' + recordId.getSobjectType().getDescribe().getName() + ' ' +
            'WHERE Id = :recordId'
        );
    }

    /*
     * @method		: createRecord()
     * @params		: N/A
     * @return 		: sObject
     * @description	: Method to insert a generic sObject and return it to an LWC
     */
    @AuraEnabled
    public static sObject createRecord(sObject record){
        // Insert the record
        insert record;
        
        // Return the record
        return record;
    }

    /*
     * @method		: updateRecord()
     * @params		: N/A
     * @return 		: sObject
     * @description	: Method to update a generic sObject and return it to an LWC
     */
    @AuraEnabled
    public static sObject updateRecord(sObject record){
        // Update the record
        update record;
        
        // Return the record
        return record;
    }
}