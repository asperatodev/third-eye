/**
 * Test class for the Asperato class
 * @author Timothy Gentet-O'Brien <tim@thirdeyeconsulting.co.uk>
 * @date 2020-08-13
 */
@isTest
public class AsperatoTest {
    /*
     * @method      : testSetup()
     * @description : Test Setup Method to create data prior to test methods running
     * @return      : void
     * @params      : N/A
     */
    @testSetup public static void testSetup(){
        // Insert Asperator Custom Setting
        insert new asp04__AsperatoOneSettings__c(
            SetupOwnerId = UserInfo.getOrganizationId(),
            asp04__BACS_Delay__c = 4,
            asp04__CreatePaymentSchedule__c = true,
            asp04__Customer_ID__c = '1367',
            asp04__Is_Live__c = false,
            asp04__Server_URL__c = 'test.protectedpayments.net'//,
            // ffps_ffaasp__EnableAutoCashProcessing__c = true,
            // ffps_ffaasp__EnableAutoPayments__c = false,
            // ffps_ffaasp__PayInFull__c = false
        );
    }

    /**
     * Test Method to test the getSettingData() method
     * 
     * @returns {void}
     */
    @isTest public static void testGetSettingsData(){
        System.assertEquals(asp04__AsperatoOneSettings__c.getInstance(), Asperato.getSettingData());
    }

    /**
     * Test Method to test the getRecord() method
     * 
     * @returns {void}
     */
    @isTest public static void testGetRecord(){
        // Create an Account with related records
        // Account acc = TestDataFactory.createAccountsWithContactsAndOpps(1).get(0);

        // Invoke the getRecord method, returning an Account
        //Account acc2 = (Account)Asperato.getRecord(acc.Id, 'Opportunities', 'Name', 'ASC', 1);

        // Assert the response is correct
        // System.assertEquals(acc.Id, acc2.Id);
    }

    /**
     * Test Method to test the createRecord() and updateRecord() methods
     * 
     * @returns {void}
     */
    @isTest public static void test(){
        // Instantiate a fake Account
        sObject acc = new Account();
        //acc.put('Id', Userinfo.getUserId());
        acc.put('Name', 'Name');

        // Invoke the createRecord() Method, passing the Account record
        sObject s = Asperato.createRecord((sObject)acc);

        // Set the Billing Street Field
        s.put('BillingStreet', 'Street');

        // Invoke the updateRecord() Method, passing the sObject returned from the createRecord() Method
        s = Asperato.updateRecord(s);

        // Assert the recrod was updated
        System.assertEquals('Street', (String)s.get('BillingStreet'));
    }
}