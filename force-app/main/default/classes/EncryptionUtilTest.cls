/**
 * Test class for the EncryptionUtil Class
 * @author Timothy Gentet-O'Brien <tim@thirdeyeconsulting.co.uk>
 * @date 2020-08-13
 */
@isTest
public class EncryptionUtilTest {
    public static String jsonObj = '{ "key": "value" }';

    /**
     * Test Method to test functionality
     * 
     * @returns {void}
     *
     */
    @isTest public static void test(){
        // Instanitate a new List of Data Apex Objects
        List<EncryptionUtil.Data> data = new List<EncryptionUtil.Data>();

        // Instantiate an EncryptionUtil Object and assign values
        EncryptionUtil eJson = new EncryptionUtil();
        eJson.aid = 'asdfghjkl';
        eJson.pid = '1234567890';
        eJson.legacyAid = '0987654321';
        
        // Instantiate a new Data Object and assign values
        EncryptionUtil.Data d = new EncryptionUtil.Data();
        d.key = Label.Encryption_Key;
        d.json = eJson;

        // Add the Data Object to the data List
        data.add(d);

        // Invoke encryptData to return the encrypted String
        String encryptedString = EncryptionUtil.encryptData(data).get(0);

        // Invoke the decrypt256() method returning an Object
        Object o = EncryptionUtil.decrypt256(encryptedString);

        EncryptionUtil e = (EncryptionUtil)JSON.deserialize(JSON.serialize(o), EncryptionUtil.class);

        // Assert the response
        System.assertEquals(eJson.pid, e.pid);
        System.assertEquals(eJson.aid, e.aid);
        System.assertEquals(eJson.legacyAid, e.legacyAid);
    }
}