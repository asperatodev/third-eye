/**
 * Util Class used to run encryption and decryption
 * @author Timothy Gentet-O'Brien <tim@thirdeyeconsulting.co.uk>
 * @date 2020-06-05
 */
public with sharing class EncryptionUtil {
    @AuraEnabled
    public String aid;
    @AuraEnabled
    public String pid;
    @AuraEnabled
    public String legacyAid;
    
    /**
     * Method to encrypt a String
     * 
     * @return {String} Encrypted String 
     * 
     * @param {String} value String to encrypt
     */
    @InvocableMethod(label = 'Encryption Utils' description = 'Encryption Utils is used to encrypt data')
    public static List<String> encryptData(List<Data> data){
        List<String> values = new List<String>();
        
        for(Data d : data){
            values.add(encrypt256(d));
        }
        
        return values;
    }

    /**
     * Method to encrypt a String
     * 
     * @return {String} Encrypted String 
     * 
     * @param {String} value String to encrypt
     */
    public static String encrypt256(Data data){
        return encrypt(
            'AES256',
            EncodingUtil.base64Decode(data.key),
            Blob.valueOf(JSON.serialize(data.json))
        );
    }

    /**
     * Method to decrypt a String
     * 
     * @return {String} Decrypted String
     * 
     * @param {String} value String to decrypt
     */
    @AuraEnabled(cacheable = true)
    public static Object decrypt256(String encryptedString){
        return JSON.deserializeUntyped(
            decrypt(
                'AES256',
                EncodingUtil.base64Decode(Label.Encryption_Key),
                EncodingUtil.base64Decode(encryptedString)
            )
        );
    }
    
    /**
     * Private method to encrypt a String
     * 
     * @return {String} Encrypted String
     * 
     * @param {String} algorithm Encryption Algorithm to use
     * @param {Blob} key Private Key to use for the encryption
     * @param {Blob} text String to encrypt
     */
    private static String encrypt(String algorithm, Blob key, Blob text) {
        return EncodingUtil.base64Encode(Crypto.encryptWithManagedIV(algorithm, key, text));
    }
    
    /**
     * Private method to decrypt a String
     * 
     * @return {String} Decrypted String
     * 
     * @param {String} algorithm Encryption Algorithm to use
     * @param {Blob} key Private Key to use for the encryption
     * @param {Blob} text String to decrypt
     */
    public static String decrypt(String algorithm, Blob key, Blob text) {
        return Crypto.decryptWithManagedIV(algorithm, key, text).toString();
    }
    
    public class Data {
        @InvocableVariable(label = 'JSON' description = 'JSON Object to be encrypted')
        public EncryptionUtil json;
        @InvocableVariable(label = 'Encrypted String' description = 'Encrypted String to be decrypted')
        public String encryptedString;
        @InvocableVariable(label = 'Key' description = 'Key to be used with encryption/decryption')
        public String key;
    }
}